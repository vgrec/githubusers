## General info ##
The project is structured in 3 main layers:


 * **The UI layer**, composed of Activities, mainly responsible for updating the UI once new data is available.
 * **The ViewModel layer**, responsible for managing UI related data and providing the UI a means
 of subscribing and listening to data changes via LiveData observable holders.
 * **The Repository layer** - the entry point for Github data. Now, in the context of this sample project
 the necessity of this layer is questionable, as all of the data is queried only from one source.
 However, I included it more for illustrative purposes, eg.: to give an idea how a real app might be structured.