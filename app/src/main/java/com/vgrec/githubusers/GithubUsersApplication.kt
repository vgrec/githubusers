package com.vgrec.githubusers

import android.app.Application
import com.vgrec.githubusers.di.networkModule
import com.vgrec.githubusers.di.uiModule
import org.koin.android.ext.android.startKoin

class GithubUsersApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(networkModule, uiModule))
    }

}