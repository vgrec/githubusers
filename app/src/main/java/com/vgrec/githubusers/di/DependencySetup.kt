package com.vgrec.githubusers.di

import com.vgrec.githubusers.service.GithubRepository
import com.vgrec.githubusers.service.GithubService
import com.vgrec.githubusers.ui.details.UserDetailViewModel
import com.vgrec.githubusers.ui.list.UserListViewModel
import com.vgrec.githubusers.utils.NetworkDetector
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.Request
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val BASE_URL = "https://api.github.com"

val uiModule = module {
    viewModel { UserListViewModel(get()) }
    viewModel { UserDetailViewModel(get()) }
}

val networkModule = module {
    single {
        Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(get())
                .build()
    }

    single {
        val retrofit: Retrofit = get()
        retrofit.create(GithubService::class.java)
    }

    single { GithubRepository(get()) }

    single { NetworkDetector(androidContext()) }

    single {
        val cacheSize = (5 * 1024 * 1024).toLong() // 5 MB
        val cacheDir = Cache(androidContext().cacheDir, cacheSize)

        OkHttpClient.Builder()
                .cache(cacheDir)
                .addInterceptor { chain ->
                    var request: Request = chain.request()
                    val networkDetector: NetworkDetector = get()

                    request = if (networkDetector.isOnline()) {
                        request.newBuilder()
                                .header("Cache-Control", "public, max-age=" + 5)
                                .build()
                    } else {
                        request.newBuilder()
                                .header("Cache-Control", "public, only-if-cached, max-stale=" + TimeUnit.DAYS.toSeconds(7))
                                .build()
                    }

                    chain.proceed(request)
                }.build()
    }

}