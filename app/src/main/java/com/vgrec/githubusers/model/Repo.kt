package com.vgrec.githubusers.model

class Repo(val name: String, val description: String)