package com.vgrec.githubusers.model

data class SearchResponse(val items: List<User>)