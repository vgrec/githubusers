package com.vgrec.githubusers.model

import com.google.gson.annotations.SerializedName

data class User(
        @SerializedName("login") val name: String,
        @SerializedName("avatar_url") val avatarUrl: String,
        @SerializedName("url") val url: String)
