package com.vgrec.githubusers.model

data class UserDetailResponse(val company: String,
                              val location: String,
                              val followers: Int,
                              val following: Int)