package com.vgrec.githubusers.service

import com.vgrec.githubusers.model.Repo
import com.vgrec.githubusers.model.SearchResponse
import com.vgrec.githubusers.model.User
import com.vgrec.githubusers.model.UserDetailResponse
import com.vgrec.githubusers.ui.model.UserDetailState
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class GithubRepository(private val githubService: GithubService) {

    fun getUserDetail(username: String): Single<UserDetailState> {
        return Single.zip(
                githubService.listRepos(username),
                githubService.getUserDetail(username),
                BiFunction<List<Repo>, UserDetailResponse, UserDetailState> { repos, userDetail ->
                    UserDetailState(repos, userDetail)
                }
        )
    }

    fun users(): Single<List<User>> = githubService.users()

    fun search(username: String): Single<SearchResponse> = githubService.search(username)

}