package com.vgrec.githubusers.service

import com.vgrec.githubusers.model.Repo
import com.vgrec.githubusers.model.SearchResponse
import com.vgrec.githubusers.model.User
import com.vgrec.githubusers.model.UserDetailResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubService {
    @GET("/users")
    fun users(): Single<List<User>>

    @GET("/search/users")
    fun search(@Query("q") username: String): Single<SearchResponse>

    @GET("/users/{user_name}/repos")
    fun listRepos(@Path("user_name") username: String): Single<List<Repo>>

    @GET("/users/{user_name}")
    fun getUserDetail(@Path("user_name") username: String): Single<UserDetailResponse>
}