package com.vgrec.githubusers.ui

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Base class for all [ViewModel] implementations responsible
 * for collecting and clearing Rx disposables in a centralized place.
 */
open class RxViewModel : ViewModel() {

    private val disposables = CompositeDisposable()

    protected fun add(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun onCleared() {
        disposables.clear()
    }
}