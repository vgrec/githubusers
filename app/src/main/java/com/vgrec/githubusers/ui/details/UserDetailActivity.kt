package com.vgrec.githubusers.ui.details

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.vgrec.githubusers.model.Repo
import com.vgrec.githubusers.utils.setTextOrHide
import com.vgrec.githubusers.utils.setVisible
import kotlinx.android.synthetic.main.activity_user_detail.*
import kotlinx.android.synthetic.main.content_user_detail.*
import kotlinx.android.synthetic.main.layout_user_info.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import vgrec.com.githubusers.R

class UserDetailActivity : AppCompatActivity() {
    private val userDetailViewModel: UserDetailViewModel by viewModel()

    private val repoList = mutableListOf<Repo>()
    private val userDetailAdapter = UserDetailAdapter(repoList)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        reposRecyclerView.adapter = userDetailAdapter
        reposRecyclerView.layoutManager = LinearLayoutManager(this)
        reposRecyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        val username = intent.getStringExtra(USERNAME)
        val avatarUrl = intent.getStringExtra(AVATAR_URL)

        supportActionBar?.title = username

        loadAvatar(avatarUrl)

        userDetailViewModel.userDetailState.observe(this, Observer { userDetail ->
            repoList.clear()
            repoList.addAll(userDetail.repos)
            userDetailAdapter.notifyDataSetChanged()

            userInfoGroup.setVisible(true)
            val userDetailData = userDetail.userDetailData

            followersFollowingTextView.text = getString(R.string.followers_and_following, userDetailData.followers, userDetailData.following)
            companyTextView.setTextOrHide(userDetailData.company)
            locationTextView.setTextOrHide(userDetailData.location)
        })

        userDetailViewModel.progressBarState.observe(this, Observer { isVisible ->
            progressBarView.setVisible(isVisible)
        })

        userDetailViewModel.getUserDetails(username)
    }

    private fun loadAvatar(avatarUrl: String) {
        Glide.with(this)
                .load(avatarUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(bigAvatarImageView)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val USERNAME = "username"
        const val AVATAR_URL = "avatar_url"
    }
}
