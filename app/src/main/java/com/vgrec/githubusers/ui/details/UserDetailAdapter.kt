package com.vgrec.githubusers.ui.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vgrec.githubusers.model.Repo
import vgrec.com.githubusers.R

class UserDetailAdapter(private val repoList: List<Repo>) : RecyclerView.Adapter<UserDetailAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_repo, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = repoList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repo: Repo = repoList[position]

        holder.repoNameView.text = repo.name
        holder.repoDescriptionView.text = repo.description
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val repoNameView = view.findViewById<TextView>(R.id.repoNameTextView)
        val repoDescriptionView = view.findViewById<TextView>(R.id.repoDescriptionTextView)
    }
}