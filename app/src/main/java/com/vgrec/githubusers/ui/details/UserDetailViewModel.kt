package com.vgrec.githubusers.ui.details

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vgrec.githubusers.service.GithubRepository
import com.vgrec.githubusers.ui.RxViewModel
import com.vgrec.githubusers.ui.model.UserDetailState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class UserDetailViewModel(private val githubRepository: GithubRepository) : RxViewModel() {

    private val _userDetailState = MutableLiveData<UserDetailState>()
    val userDetailState: LiveData<UserDetailState>
        get() = _userDetailState

    private val _progressBarState = MutableLiveData<Boolean>()
    val progressBarState: LiveData<Boolean>
        get() = _progressBarState

    fun getUserDetails(username: String) {
        add(githubRepository.getUserDetail(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .toObservable()
                .doOnTerminate { _progressBarState.postValue(false) }
                .doOnSubscribe { _progressBarState.postValue(true) }
                .subscribe({ detailState -> _userDetailState.postValue(detailState) },
                        { error -> Log.e(TAG, "An error occured when loading the repos", error) }))
    }

    companion object {
        val TAG = UserDetailViewModel::class.java.simpleName
    }
}