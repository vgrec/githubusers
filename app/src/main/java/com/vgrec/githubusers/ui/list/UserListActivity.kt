package com.vgrec.githubusers.ui.list

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.vgrec.githubusers.model.User
import com.vgrec.githubusers.ui.details.UserDetailActivity
import com.vgrec.githubusers.utils.setVisible
import kotlinx.android.synthetic.main.activity_user_list.*
import kotlinx.android.synthetic.main.layout_empty_view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import vgrec.com.githubusers.R

class UserListActivity : AppCompatActivity() {
    private val userListViewModel: UserListViewModel by viewModel()

    private val userList = mutableListOf<User>()
    private val userListAdapter = UserListAdapter(userList) { user, avatar ->
        openDetailsActivity(user, avatar)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)
        setSupportActionBar(toolbar)

        usersRecyclerView.adapter = userListAdapter
        usersRecyclerView.layoutManager = LinearLayoutManager(this)
        usersRecyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        searchView.layoutParams = Toolbar.LayoutParams(Gravity.END)

        userListViewModel.observeSearchView(searchView)

        userListViewModel.usersState.observe(this, Observer { userListResponse ->
            userList.clear()
            userList.addAll(userListResponse)
            userListAdapter.notifyDataSetChanged()
        })

        userListViewModel.progressBarState.observe(this, Observer { isVisible ->
            progressBarView.setVisible(isVisible)
        })

        userListViewModel.emptyViewState.observe(this, Observer { emptyViewState ->
            emptyView.setVisible(emptyViewState.visible)
            emptyStateTextView.setText(emptyViewState.stringRes)
        })

        userListViewModel.getUsers()
    }

    override fun onBackPressed() {
        if (!searchView.isIconified) {
            searchView.isIconified = true
        } else {
            super.onBackPressed()
        }
    }

    private fun openDetailsActivity(user: User, avatar: ImageView) {
        val intent = Intent(this, UserDetailActivity::class.java)
        intent.putExtra(UserDetailActivity.USERNAME, user.name)
        intent.putExtra(UserDetailActivity.AVATAR_URL, user.avatarUrl)

        val transitionName = getString(R.string.activity_image_transition)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                avatar,
                transitionName)

        startActivity(intent, options.toBundle())
    }
}
