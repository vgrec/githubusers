package com.vgrec.githubusers.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.vgrec.githubusers.model.User
import vgrec.com.githubusers.R

class UserListAdapter(private val userList: List<User>,
                      private val clickListener: (User, ImageView) -> Unit) : RecyclerView.Adapter<UserListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = userList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user: User = userList[position]

        holder.userNameView.text = user.name
        holder.userUrlView.text = user.url
        Glide.with(holder.userAvatarView)
                .load(user.avatarUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.userAvatarView)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        init {
            view.setOnClickListener(this)
        }

        val userNameView = view.findViewById<TextView>(R.id.usernameTextView)
        val userAvatarView = view.findViewById<ImageView>(R.id.smallAvatarImageView)
        val userUrlView = view.findViewById<TextView>(R.id.userUrlTextView)

        override fun onClick(v: View?) {
            clickListener(userList[adapterPosition], userAvatarView)
        }
    }
}