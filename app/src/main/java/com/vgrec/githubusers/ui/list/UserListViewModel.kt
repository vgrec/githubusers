package com.vgrec.githubusers.ui.list

import android.widget.SearchView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jakewharton.rxbinding2.widget.RxSearchView
import com.vgrec.githubusers.model.User
import com.vgrec.githubusers.service.GithubRepository
import com.vgrec.githubusers.service.GithubService
import com.vgrec.githubusers.ui.RxViewModel
import com.vgrec.githubusers.ui.model.EmptyViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import vgrec.com.githubusers.R
import java.util.concurrent.TimeUnit

class UserListViewModel(private val githubRepository: GithubRepository) : RxViewModel() {

    private val _usersState = MutableLiveData<List<User>>()
    val usersState: LiveData<List<User>>
        get() = _usersState

    /**
     * true - the progress bar is visible, false - is hidden
     */
    private val _progressBarState = MutableLiveData<Boolean>()
    val progressBarState: LiveData<Boolean>
        get() = _progressBarState

    /**
     * true - the empty state is visible, false - is hidden
     */
    private val _emptyViewState = MutableLiveData<EmptyViewState>()
    val emptyViewState: LiveData<EmptyViewState>
        get() = _emptyViewState

    fun getUsers() {
        add(githubRepository.users()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .toObservable()
                .doOnTerminate { _progressBarState.postValue(false) }
                .doOnSubscribe { _progressBarState.postValue(true) }
                .subscribe(successListener, errorListener))
    }

    fun search(username: String) {
        add(githubRepository.search(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .toObservable()
                .doOnTerminate { _progressBarState.postValue(false) }
                .doOnSubscribe { _progressBarState.postValue(true) }
                .map { it.items }
                .subscribe(successListener, errorListener))
    }

    fun observeSearchView(searchView: SearchView) {
        add(RxSearchView.queryTextChangeEvents(searchView)
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(300, TimeUnit.MILLISECONDS)
                .filter { it.queryText().length >= 3 }
                .subscribe { search(it.queryText().toString()) })
    }

    private val successListener: (List<User>) -> Unit = {
        _emptyViewState.postValue(EmptyViewState(it.isEmpty(), R.string.empty_state_no_users))
        _usersState.postValue(it)
    }

    private val errorListener: (Throwable) -> Unit = {
        _emptyViewState.postValue(EmptyViewState(true, R.string.empty_state_error))
    }
}