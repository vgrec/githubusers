package com.vgrec.githubusers.ui.model

import androidx.annotation.StringRes

data class EmptyViewState(val visible: Boolean, @StringRes val stringRes: Int)