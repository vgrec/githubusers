package com.vgrec.githubusers.ui.model

import com.vgrec.githubusers.model.Repo
import com.vgrec.githubusers.model.UserDetailResponse

class UserDetailState(val repos: List<Repo>, val userDetailData: UserDetailResponse)