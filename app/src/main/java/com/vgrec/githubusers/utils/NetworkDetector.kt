package com.vgrec.githubusers.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class NetworkDetector(private val context: Context) {
    fun isOnline(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }
}