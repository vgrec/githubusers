package com.vgrec.githubusers.utils

import android.view.View
import android.widget.TextView

/**
 * Extension function that allows setting the visibility of any view
 * based on a boolean flag.
 */
fun View.setVisible(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

/**
 * Simple extension function that hides the [TextView] if the passed
 * text is empty or null.
 */
fun TextView.setTextOrHide(text: String?) {
    if (text.isNullOrEmpty()) {
        this.setVisible(false)
    } else {
        this.text = text
    }
}