package vgrec.com.githubusers

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.vgrec.githubusers.model.User
import com.vgrec.githubusers.service.GithubRepository
import com.vgrec.githubusers.ui.list.UserListViewModel
import com.vgrec.githubusers.ui.model.EmptyViewState
import io.reactivex.Single
import junit.framework.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import vgrec.com.githubusers.util.LiveDataTestUtil
import vgrec.com.githubusers.util.RxSchedulerRule

class UserListViewModelTest {
    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    private val githubRepository: GithubRepository = mock(GithubRepository::class.java)
    private val userListViewModel = UserListViewModel(githubRepository)

    private var testUser = User("tomcat", "http://avatar", "http://details")
    private val testObservable = Single.just(listOf(testUser))

    @Test
    fun testGetUsers_Success() {
        `when`(githubRepository.users()).thenReturn(testObservable)

        userListViewModel.getUsers()

        // Make sure the the LiveData is updated with the user list
        val users: List<User> = LiveDataTestUtil.getValue(userListViewModel.usersState)
        assertEquals(testUser, users[0])

        // Make sure the empty state view is hidden
        val emptyViewState: EmptyViewState = LiveDataTestUtil.getValue(userListViewModel.emptyViewState)
        assertFalse(emptyViewState.visible)
    }

    @Test
    fun testGetUsers_Error() {
        `when`(githubRepository.users()).thenReturn(Single.error(Throwable()))

        userListViewModel.getUsers()

        // Make sure the empty state view is visible
        val emptyViewState: EmptyViewState = LiveDataTestUtil.getValue(userListViewModel.emptyViewState)
        assertTrue(emptyViewState.visible)
    }
}